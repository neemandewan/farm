app.service('tabsService', [tabsService]);
function tabsService() {
    var a = {
        "tabs": [
            {
                "title": "National",
                "iconOn": "ion-ios-box",
                "iconOff": "ion-ios-box-outline",
                "url": "templates/tabs/national-b.html",
                "href": "national"
            },
            {
                "title": "International",
                "iconOn": "ion-ios-filing",
                "iconOff": "ion-ios-filing-outline",
                "url": "templates/tabs/international-b.html",
                "href": "inetrnational"
            }
        ]
    };
    return {
        loadAllItems : function() {
            return a;
        }
    };
}

app.service("wordpress",['$q','$http','errorHandler','$ionicLoading', '$httpParamSerializerJQLike', 
    function($q, $http, errorHandler, $ionicLoading, $httpParamSerializerJQLike) {

        var self = this;
        var url = 'https://www.farm.com.np';

        //Example
        // wp-json/wc/v1/products?oauth_consumer_key=ck_c6a2b08b9789d76d2dfbcfcfee0f87e3e982ecbf&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1479988705&oauth_nonce=iBq9sF&oauth_version=1.0&oauth_signature=sgsc7rgVKtM4xj4lsyG3D9wKOJA= HTTP/1.1

        //OAuth Protocol authentication parameters
        /*var oauth = new OAuth({
            consumer: {
                public: 'ck_7b1e4751d66d057de326c20acd6c8c8b',    //Consumer Public Key
                secret: 'cs_65ff74ce0aae6761afbe0a3ee16d0a48'    //Consumer Secrete Key
            },
            signature_method: 'HMAC-SHA1', //oauth1.0a protocol signature method
            hash_function: function(base_string, key) {
                return crypto.createHmac('sha1', key).update(base_string).digest('base64');
            }
        });*/

        var request = { method: 'GET' },
            key,
            tokenSecret = '',
            oauth_data ={},
            deff;

        this.sendPushToken = function(token, os) {
            
            request.url = 'https://farm.com.np/wp-json/apnwp/register?os_type='+os+'&&device_token='+ token;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                //$ionicLoading.hide();
                deff.resolve(objS);
                
            },function(objE) {

                //$ionicLoading.hide();
                objE = JSON.stringify(objE);

                errorHandler.serverErrorhandler(objE);
                deff.reject("server Error");
            });

            return deff.promise;
        };

        this.postPushToken = function(options) {
            
            request.url = 'https://farm.com.np/pnfw/register/';
            //request.method = 'POST';

            deff = $q.defer();

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0'
            };*/

            //oauth_data = {};

            /*var checkEmpty = angular.equals(options, {});
            if(!checkEmpty) {
                for (key in options) {
                    if (options.hasOwnProperty(key)) {
                        oauth_data[key] = options[key];
                    }
                }
            }*/

            /*console.log("postOrder -- >>> ");
            console.log(oauth_data);

            // Oauth signature
            oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});*/

            $http({
                method: 'POST',
                url: request.url,
                headers: {
                    "Content-Type":"application/x-www-form-urlencoded"
                },
                data: $httpParamSerializerJQLike(options)
            }).then(function(objS) {

                //$ionicLoading.hide();
                deff.resolve(objS);
                
            },function(objE) {

                //$ionicLoading.hide();
                objE = JSON.stringify(objE);

                errorHandler.serverErrorhandler(objE);
                deff.reject("server Error");
            });

            return deff.promise;
        };
  
        //Service Function to get products
        this.getAllPosts = function(options) {
            
            request.url = url + '/wp-json/wp/v2/posts';

            if(!options.per_page || options.per_page == null) per_page = 10;
            if(!options.page || options.page == null) page = 1;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            var checkEmpty = angular.equals(options, {});
            if(!checkEmpty) {
                for (key in options) {
                    if (options.hasOwnProperty(key)) {
                        oauth_data[key] = options[key];
                    }
                }
            }

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                //$ionicLoading.hide();
                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE);

                errorHandler.serverErrorhandler(objE);
                deff.reject("server Error");
            });

            return deff.promise;
        };

        this.getPost = function(id) {
            
            request.url = url + '/wp-json/wp/v2/posts/' + id;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE);

                errorHandler.serverErrorhandler(objE);
                deff.reject("server Error");
            });

            return deff.promise;
        };

        this.getAllNews = function(options) {
            
            request.url = url + '/wp-json/wp/v2/posts';

            if(!options.per_page || options.per_page == null) per_page = 10;
            if(!options.page || options.page == null) page = 1;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            var checkEmpty = angular.equals(options, {});
            if(!checkEmpty) {
                for (key in options) {
                    if (options.hasOwnProperty(key)) {
                        oauth_data[key] = options[key];
                    }
                }
            }

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE);

                if(objE.data.message != undefined) {
                    if(objE.data.message == 'The page number requested is larger than the number of pages available.') {
                        deff.reject("load complete");
                    }
                }else {
                    errorHandler.serverErrorhandler(objE);
                    deff.reject("server Error");
                
                }
            });

            return deff.promise;
        };

        this.getPages = function(id) {
            
            request.url = url + '/wp-json/wp/v2/pages/' + id;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE);

                if(objE.data.message != undefined) {
                    if(objE.data.message == 'The page number requested is larger than the number of pages available.') {
                        deff.reject("load complete");
                    }
                }else {
                    errorHandler.serverErrorhandler(objE);
                    deff.reject("server Error");
                
                }
            });

            return deff.promise;
        };

        this.getTechnicalPub = function(options) {
            
            request.url = url + '/wp-json/wp/v2/articles';

            if(!options.per_page || options.per_page == null) per_page = 10;
            if(!options.page || options.page == null) page = 1;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            var checkEmpty = angular.equals(options, {});
            if(!checkEmpty) {
                for (key in options) {
                    if (options.hasOwnProperty(key)) {
                        oauth_data[key] = options[key];
                    }
                }
            }

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE);
                objE = JSON.parse(objE);

                if(objE.data.message != undefined) {
                    if(objE.data.message == 'The page number requested is larger than the number of pages available.') {
                        deff.reject("load complete");
                    }
                }else {
                    errorHandler.serverErrorhandler(objE);
                    deff.reject("server Error");
                
                }
            });

            return deff.promise;
        };

        this.getResearchPub = function(options) {
            
            request.url = url + '/wp-json/wp/v2/research';

            if(!options.per_page || options.per_page == null) per_page = 10;
            if(!options.page || options.page == null) page = 1;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            var checkEmpty = angular.equals(options, {});
            if(!checkEmpty) {
                for (key in options) {
                    if (options.hasOwnProperty(key)) {
                        oauth_data[key] = options[key];
                    }
                }
            }

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE);
                objE = JSON.parse(objE);

                if(objE.data.message != undefined) {
                    if(objE.data.message == 'The page number requested is larger than the number of pages available.') {
                        deff.reject("load complete");
                    }
                }else {
                    errorHandler.serverErrorhandler(objE);
                    deff.reject("server Error");
                
                }
            });

            return deff.promise;
        };

        this.getStudentPub = function(options) {
            
            request.url = url + '/wp-json/wp/v2/student_articles';

            if(!options.per_page || options.per_page == null) per_page = 10;
            if(!options.page || options.page == null) page = 1;

            deff = $q.defer();

            /*$ionicLoading.show({
                template: '<ion-spinner class="light"></ion-spinner>'
            });*/

            //OAuth Parameters to call woocommerce api      
            /*oauth_data = {
                oauth_consumer_key: oauth.consumer.public,
                oauth_signature_method: oauth.signature_method,
                oauth_timestamp: oauth.getTimeStamp(),
                oauth_nonce: oauth.getNonce(),
                oauth_version: '1.0',
                orderby: "date"
            };*/

            oauth_data = {};

            var checkEmpty = angular.equals(options, {});
            if(!checkEmpty) {
                for (key in options) {
                    if (options.hasOwnProperty(key)) {
                        oauth_data[key] = options[key];
                    }
                }
            }

            // Oauth signature
            //oauth_data.oauth_signature = oauthSignature.generate(request.method, request.url, oauth_data, oauth.consumer.secret, tokenSecret, {encodeSignature: false});

            $http({
                method: request.method,
                url: request.url,
                headers: {
                    "Content-Type":"application/JSON"
                },
                params: oauth_data
            }).then(function(objS) {

                deff.resolve(objS);
                
            },function(objE) {

                $ionicLoading.hide();
                objE = JSON.stringify(objE)
                objE = JSON.parse(objE);

                if(objE.data.message != undefined) {
                    if(objE.data.message == 'The page number requested is larger than the number of pages available.') {
                        deff.reject("load complete");
                    }
                }else {
                    errorHandler.serverErrorhandler(objE);
                    deff.reject("server Error");
                
                }
            });

            return deff.promise;
        };
    }
]);

app.service('errorHandler',['$q',function($q) {
    this.serverErrorhandler=function(error){
        console.log("ERROR ::"+JSON.stringify(error));
    };
}]);

app.filter('removeHTMLTags', function() {
    return function(text) {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});

app.filter('removeReadmore', function() {
    return function(text) {
        return  text ? String(text).replace('Read More', '') : '';
    };
});

app.filter('removeTime', function() {
    return function(text) {
        return  text ? String(text).split("T")[0] : '';
    };
});

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});