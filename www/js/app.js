// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', [
  'ionic', 
  'ionic-material', 
  'ngCookies', 
  'base64', 
  'ngSanitize', 
  'ngCordova', 
  'ionic-toast',
  'ionic.cloud',
  'ngCordova.plugins.nativeStorage']);

app.run(function($ionicPlatform, $ionicPopup, $rootScope, $cordovaSplashscreen, $ionicHistory, $timeout, $ionicPush, ionicToast) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    /*if (window.statusbarTransparent) {
      statusbarTransparent.enable();
    }*/

    /*var push = new Ionic.Push({
      "debug": true
    });
 
    push.register(function(token) {
      console.log("My Device token:", token.token);
      push.saveToken(token);  // persist the token in the Ionic Platform
    });*/
    
    /*POP UP TO CHECK INTERNET CONNECTION*/
    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        $ionicPopup.confirm({
          title: 'No internet connection',
          cssClass: 'sh-s',
          subTitle: '',
          template: 'The internet is disconnected on your device.',
          templateUrl: '',
          cancelText: 'OK',
          cancelType: 'button-assertive'
        })
        .then(function(result) {
          if(!result) {
            ionic.Platform.exitApp();
          }
        });
      }
    }

    /*BACK BUTTON TWICE TO EXIT APP*/
    $ionicPlatform.registerBackButtonAction(function(e){
      if ($rootScope.backButtonPressedOnceToExit) {
        ionic.Platform.exitApp();
      }

      else if ($ionicHistory.backView()) {
        $ionicHistory.goBack();
      }
      else {
        $rootScope.backButtonPressedOnceToExit = true;
        //ionicToast.show('Press back button again to exit', 'bottom', true, 5000);
        window.plugins.toast.showShortBottom(
          "Press back button again to exit",function(a){},function(b){}
        );
        setTimeout(function(){
          $rootScope.backButtonPressedOnceToExit = false;
        },2000);
      }
      e.preventDefault();
      return false;
    },101);
  });

});

app.config(function($ionicCloudProvider) {
  $ionicCloudProvider.init({
    insights: {
        enabled: false
    },
    "core": {
      "app_id": "b1427073"
    },
    "push": {
      "sender_id": "534054042614",
      "pluginConfig": {
        "ios": {
          "badge": true,
          "sound": true
        },
        "android": {
          "iconColor": "#343434"
        }
      }
    }
  });
})

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('app.whoweare', {
    url: '/whoweare',
    views: {
      'menuContent': {
        templateUrl: 'templates/whoweare.html',
        controller: 'whoWeAreCtrl'
      }
    }
  })

  .state('app.messagefromfarm', {
    url: '/messagefromfarm',
    views: {
      'menuContent': {
        templateUrl: 'templates/messagefromfarm.html',
        controller: 'messageFromFarmCtrl'
      }
    }
  })

  .state('app.pdl', {
    url: '/pdl',
    views: {
      'menuContent': {
        templateUrl: 'templates/pdl.html',
        controller: 'pdlCtrl'
      }
    }
  })

  .state('app.businessdirectory', {
    url: '/businessdirectory',
    views: {
      'menuContent': {
        templateUrl: 'templates/businessdirectory.html',
        controller: 'businessDirectoryCtrl'
      }
    }
  })

  .state('app.poultrystatistics', {
    url: '/poultrystatistics',
    views: {
      'menuContent': {
        templateUrl: 'templates/poultrystatistics.html',
        controller: 'poultryStatisticsCtrl'
      }
    }
  })

  .state('app.advertisewithus', {
    url: '/advertisewithus',
    views: {
      'menuContent': {
        templateUrl: 'templates/advertisewithus.html',
        controller: 'advertiseWithUs'
      }
    }
  })

  .state('app.writetous', {
    url: '/writetous',
    views: {
      'menuContent': {
        templateUrl: 'templates/writetous.html',
        controller: 'writeToUsCtrl'
      }
    }
  })


  .state('app.callus', {
    url: '/callus',
    views: {
      'menuContent': {
        templateUrl: 'templates/callus.html',
        controller: 'callUsCtrl'
      }
    }
  })

  .state('app.news', {
    url: '/news',
    views: {
      'menuContent': {
        templateUrl: 'templates/news.html',
        controller: 'newsCtrl'
      }
    }
  })

  .state('app.newsdetail', {
    url: '/news/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/newsdetail.html',
        controller: 'newsDetailCtrl'
      }
    }
  })

  .state('app.poultrybasic', {
    url: '/poultrybasic',
    views: {
      'menuContent': {
        templateUrl: 'templates/poultrybasic.html',
        controller: 'poultryBasicCtrl'
      }
    }
  })

  .state('app.farmhelpdesk', {
    url: '/farmhelpdesk',
    views: {
      'menuContent': {
        templateUrl: 'templates/farmhelpdesk.html',
        controller: 'farmerHelpCtrl'
      }
    }
  })

  .state('app.market', {
    url: '/market',
    views: {
      'menuContent': {
        templateUrl: 'templates/market.html',
        controller: 'marketCtrl'
      }
    }
  })

  .state('app.publication', {
    url: '/publication',
    views: {
      'menuContent': {
        templateUrl: 'templates/publication.html',
        controller: 'publicationCtrl'
      }
    }
  })

  .state('app.technical', {
    url: '/publication/technical',
    views: {
      'menuContent': {
        templateUrl: 'templates/technical.html',
        controller: 'technicalCtrl'
      }
    }
  })

  .state('app.student', {
    url: '/publication/student',
    views: {
      'menuContent': {
        templateUrl: 'templates/student.html',
        controller: 'studentCtrl'
      }
    }
  })

  .state('app.research', {
    url: '/publication/research',
    views: {
      'menuContent': {
        templateUrl: 'templates/research.html',
        controller: 'researchCtrl'
      }
    }
  })

  .state('app.detail', {
    url: '/publication/{text}/detail',
    views: {
      'menuContent': {
        templateUrl: 'templates/detail.html',
        controller: 'detailCtrl'
      }
    }
  })

  .state('app.blp', {
    url: '/poultrybasic/{data}',
    views: {
      'menuContent': {
        templateUrl: 'templates/blp.html',
        controller: 'blpCtrl'
      }
    }
  })

  .state('app.management', {
    url: '/poultrybasic/management',
    views: {
      'menuContent': {
        templateUrl: 'templates/management.html',
        controller: 'managementCtrl'
      }
    }
  })

  .state('app.disease', {
    url: '/poultrybasic/{url}/disease',
    views: {
      'menuContent': {
        templateUrl: 'templates/disease.html',
        controller: 'diseaseCtrl'
      }
    }
  })

  .state('app.lightmanagement', {
    url: '/poultrybasic/{url}/lightmanagement',
    views: {
      'menuContent': {
        templateUrl: 'templates/lightmanagement.html',
        controller: 'lightMgmtCtrl'
      }
    }
  })

  .state('app.fwm', {
    url: '/poultrybasic/{url}/fwm',
    views: {
      'menuContent': {
        templateUrl: 'templates/fwm.html',
        controller: 'fwmCtrl'
      }
    }
  })

  .state('app.poultryanatomy', {
    url: '/poultrybasic/{url}/poultryanatomy',
    views: {
      'menuContent': {
        templateUrl: 'templates/poultryanatomy.html',
        controller: 'poultryAnatomyCtrl'
      }
    }
  })

  .state('app.normalmedication', {
    url: '/poultrybasic/{url}/normalmedication',
    views: {
      'menuContent': {
        templateUrl: 'templates/normalmed.html',
        controller: 'normalMedCtrl'
      }
    }
  })

  .state('app.spacecalculator', {
    url: '/poultrybasic/{url}/spacecalculator',
    views: {
      'menuContent': {
        templateUrl: 'templates/spacecalculator.html',
        controller: 'spaceCalcCtrl'
      }
    }
  })

  .state('app.vac', {
    url: '/poultrybasic/{url}/vac',
    views: {
      'menuContent': {
        templateUrl: 'templates/vac.html',
        controller: 'vacCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});