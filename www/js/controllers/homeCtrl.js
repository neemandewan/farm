app.controller('homeCtrl', function($scope, $rootScope, $state, $location, $ionicModal, $ionicHistory, $timeout, $ionicTabsDelegate, $cordovaNativeStorage, $ionicLoading, $ionicSideMenuDelegate, ionicMaterialInk, ionicToast, wordpress) {

    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    $scope.items = [];

    $scope.swiperOptions = {
        autoplay: 2500,
        loop: true,
        speed: 1000
    }

    /*$ionicHistory.nextViewOptions({
        disableAnimate: true,
        historyRoot: true
    }); */

    $scope.doRefresh = function () {

        if($rootScope.postsPageShown == 0) {
            $rootScope.postsPageShown = 1;
        }

        opt = {
            per_page: 5,
            page: 1,
            categories: 28
        }

        wordpress.getAllPosts(opt).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);

            $ionicLoading.hide();

            $scope.items = objS.data;
            $rootScope.dashVal =  $scope.items;

            $cordovaNativeStorage.setItem("dash", $scope.items).then(function (value) {

            }, function (error) {
                console.log(error);
            });

            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {
            ionicToast.show('Oops! something went wrong...', 'bottom', false, 3000);
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    $scope.getItem = function (data) {
        $rootScope.newsdetail = data;
        $state.go("app.newsdetail", { id: data.id });
    }

    $scope.goToUrl = function (data) {

        switch(data) {
            case "likefbpage":
                window.open($rootScope.fbUrl, '_system');
                break;
            default:
                $state.go("app." + data);
        }
    }

    $cordovaNativeStorage.getItem("dash").then(function (value) {

        if(value == null) {
            $scope.doRefresh();
            return;
        }

        var dash = value;
        $scope.items = value;
        $rootScope.dashVal = dash;

    }, function (error) {

        $scope.doRefresh();
    });
    
});