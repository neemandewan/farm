app.controller('detailCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, $stateParams, $timeout, ionicMaterialInk, wordpress) {
	
	// Enable  Ink
    ionicMaterialInk.displayEffect();

    var title = $stateParams.text
    title = title.charAt(0).toUpperCase() + title.slice(1);
    $scope.title = title;
    $scope.item = [];
    $scope.error = "";

    $timeout(function () {

        $scope.item = $rootScope.detail;
        if($scope.item == null) {
            $scope.error = "No detail is available right now. May be due to internet connection. Please go back and try again.";
        }

    }, 800);

});