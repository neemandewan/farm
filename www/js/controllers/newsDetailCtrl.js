app.controller('newsDetailCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, $stateParams, $timeout, ionicMaterialInk, wordpress) {
	
	// Enable  Ink
    ionicMaterialInk.displayEffect();
    $scope.id = $stateParams.id;

    $scope.doRefresh = function() {
    	// Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getPost($scope.id).then(function(objS) {

            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            var data = objS.data;

            $timeout(function() {
            	$scope.item = data;
	            $rootScope.newsdetail = $scope.item;

            }, 0);

        },function(err) {

            console.log(err);
            $scope.error = "Server error. Please reload";

            return;
        });
    }

    if($rootScope.newsdetail == null) {
    	$scope.doRefresh();
    } else {
    	$scope.item = $rootScope.newsdetail;
    }

});