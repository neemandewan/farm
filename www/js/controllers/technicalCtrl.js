app.controller('technicalCtrl', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    $scope.moreTechnicalCanBeLoaded = true;

    $scope.swiperOptions = {
        autoplay: 2500,
        loop: true,
        speed: 1000
    }

    $scope.technical = [];
    var technical,
        opt,
        i;

    $scope.loadMoretechnical = function() {
        $scope.doRefresh();
    }

    $scope.doRefresh = function() {

        if($rootScope.technicalPageShown == 0) {
            $rootScope.technicalPageShown = 1;
        } else {
            $rootScope.technicalPageShown = $rootScope.technicalPageShown + 1;
        }

        opt = {
            per_page: $rootScope.technicalPerPage,
            page: $rootScope.technicalPageShown
        }

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getTechnicalPub(opt).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            technical = objS.data;

            if(technical[0] == undefined) {
                $scope.moreTechnicalCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            for(i=0; i<technical.length;i++) {
                $scope.technical.push(technical[i]);
            }

            //$scope.technical = technical;
            $rootScope.technical = $scope.technical;

            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            if(err == "load complete") {
                $scope.moreTechnicalCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');
            $rootScope.technicalPageShown = $rootScope.technicalPageShown - 1;

            return;
        });

    }

    $scope.goToUrl = function (data) {
        $rootScope.detail = data;
        $state.go("app.detail", { text: 'technical' });
    }
});