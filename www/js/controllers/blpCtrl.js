app.controller('blpCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, ionicMaterialInk, $stateParams, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    var url = $stateParams.data;
    if(url == undefined || url == null) {
        url == "broilers";
    }
    
    $scope.title = url;

    $scope.goToUrl = function (data) {
        $state.go("app." + data, {url: $scope.title});
    }
});