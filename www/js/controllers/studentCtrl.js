app.controller('studentCtrl', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    $scope.moreStudentCanBeLoaded = true;

    $scope.swiperOptions = {
        autoplay: 2500,
        loop: true,
        speed: 1000
    }

    $scope.loadMorestudent = function() {

        $scope.doRefresh();
    }

    $scope.student = [];
    var student,
        opt,
        i;

    $scope.doRefresh = function() {

        if($rootScope.studentPageShown == 0) {
            $rootScope.studentPageShown = 1;
        } else {
            $rootScope.studentPageShown = $rootScope.studentPageShown + 1;
        }

        opt = {
            per_page: $rootScope.studentPerPage,
            page: $rootScope.studentPageShown
        }

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getStudentPub(opt).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            student = objS.data;

            if(student[0] == undefined) {
                $scope.moreStudentCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            for(i=0; i<student.length;i++) {
                $scope.student.push(student[i]);
            }

            //$scope.student = student;
            $rootScope.student = $scope.student;

            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            if(err == "load complete") {
                $scope.moreStudentCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');
            $rootScope.studentPageShown = $rootScope.studentPageShown - 1;

            return;
        });

    }

    $scope.goToUrl = function (data) {
        $rootScope.detail = data;
        $state.go("app.detail", { text: 'student' });
    }
});