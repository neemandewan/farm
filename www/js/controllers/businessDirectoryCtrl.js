app.controller('businessDirectoryCtrl', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, tabsService, $timeout, $ionicTabsDelegate, wordpress) {
    // Tabs are defined here
    var tabsService = tabsService.loadAllItems();
    $scope.tabs = tabsService.tabs;

    var nationalBusinessId = 288;
    var internationalBusinessId = 288;

    $scope.tabSelected = function(data, index) {
        $scope.selectedTab = data;
        $ionicTabsDelegate.select(index);
        
        switch(index) {
        	case 0:
                $scope.national();
                break;
            case 1:
                $scope.international();
                break;
        }
    }

    // Initialize default tab selected
    $timeout( function() {
        $scope.tabSelected($scope.tabs[0], 0);
    }, 0);

    $scope.national = function (argument) {
    	console.log("national")
    }

    $scope.international = function (argument) {
    	console.log("inter")
    }
});