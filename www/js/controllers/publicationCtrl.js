app.controller('publicationCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, ionicMaterialInk, wordpress) {
   	

    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.goToUrl = function (data) {
        $state.go("app." + data);
    }

});