app.controller('researchCtrl', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    $scope.moreResearchCanBeLoaded = true;

    $scope.swiperOptions = {
        autoplay: 2500,
        loop: true,
        speed: 1000
    }

    $scope.loadMoreresearch = function() {

        $scope.doRefresh();
    }

    $scope.research = [];
    var research,
        opt,
        i;

    $scope.doRefresh = function() {

        if($rootScope.researchPageShown == 0) {
            $rootScope.researchPageShown = 1;
        } else {
            $rootScope.researchPageShown = $rootScope.researchPageShown + 1;
        }

        opt = {
            per_page: $rootScope.researchPerPage,
            page: $rootScope.researchPageShown
        }

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getResearchPub(opt).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            research = objS.data;

            if(research[0] == undefined) {
                $scope.moreResearchCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            for(i=0; i<research.length;i++) {
                $scope.research.push(research[i]);
            }

            //$scope.research = research;
            $rootScope.research = $scope.research;

            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            if(err == "load complete") {
                $scope.moreResearchCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');

            return;
        });

    }

    $scope.goToUrl = function (data) {
        $rootScope.detail = data;
        $state.go("app.detail", { text: 'research' });
    }
});