app.controller('messageFromFarmCtrl', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    var messageFromFarmId = 49;
    $scope.item = [];

    $scope.doRefresh = function() {

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getPages(messageFromFarmId).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            $scope.item = objS.data;

            $rootScope.item = $scope.item;
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');

            return;
        });

    }

    $scope.doRefresh();

    $scope.to_trusted = function(html_code) {
    	return $sce.trustAsHtml(html_code);
	}
});