app.controller('advertiseWithUs', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    $scope.item = [];
    var advertiseWithUsId = 226;

    $scope.doRefresh = function() {

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getPages(advertiseWithUsId).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            $scope.item = objS.data;

            $rootScope.item = $scope.item;
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');

            return;
        });

    }

    $scope.doRefresh();
});