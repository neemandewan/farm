app.controller('poultryBasicCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, ionicMaterialInk, wordpress) {
    
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.goToUrl = function (data) {
        switch(data) {
            case "broilers":
                $state.go("app.blp", {data: "broilers"} );
                break;
            case "layers":
                $state.go("app.blp", {data: "layers"} );
                break;
            case "parents":
                $state.go("app.blp", {data: "parents"} );
                break;
            case "farmhelpdesk":
            case "market":
            case "management":
                $state.go("app." + data);
                break;
            case "likefbpage":
                window.open($rootScope.fbUrl, '_system');
                break;
            default:
                $state.go("app.blp", {data: "broilers"} );
        }
    }
});