app.controller('whoWeAreCtrl', function($scope, $state, $rootScope, $ionicHistory, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    var whoWeAreId = 47;

    $scope.doRefresh = function() {

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getPages(whoWeAreId).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            $scope.whoweare = objS.data;

            $rootScope.whoweare = $scope.whoweare;
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');

            return;
        });

    }

    $scope.doRefresh();

    $scope.to_trusted = function(html_code) {
    	return $sce.trustAsHtml(html_code);
	}
});