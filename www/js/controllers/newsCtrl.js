app.controller('newsCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, ionicMaterialInk, wordpress) {
	
	// Enable  Ink
    ionicMaterialInk.displayEffect();

    $scope.error = "";
    $scope.moreNewsCanBeLoaded = true;

    $scope.loadMoreNews = function() {

        $scope.doRefresh();
    }

    $scope.swiperOptions = {
        autoplay: 2500,
        loop: true,
        speed: 1000
    }

    $scope.news = [];
    var news,
        opt,
        i;

    $scope.doRefresh = function() {

        $rootScope.postsPageShown = $rootScope.postsPageShown + 1;

        opt = {
            per_page: $rootScope.postsPerPage,
            page: $rootScope.postsPageShown,
            categories: 26
        }

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getAllNews(opt).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            news = objS.data;

            if(news[0] == undefined) {
                $scope.moreNewsCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            for(i=0; i<news.length;i++) {
                $scope.news.push(news[i]);
            }

            //$scope.news = news;
            $rootScope.news = $scope.news;

            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            if(err == "load complete") {
                $scope.moreNewsCanBeLoaded = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
                
                return;
            }

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');
            $rootScope.postsPageShown = $rootScope.postsPageShown - 1;

            return;
        });

    }

    $scope.goToUrl = function (data) {
    	$rootScope.newsdetail = data;
        $state.go("app.newsdetail", { id: data.id });
    }

});