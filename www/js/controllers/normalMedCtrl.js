app.controller('normalMedCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicLoading, $stateParams, ionicMaterialInk, wordpress) {
    // Enable  Ink
    ionicMaterialInk.displayEffect();
    $scope.error = "";

    $scope.title = $stateParams.url;
    console.log($scope.title);

    $scope.item = [];

    switch($scope.title) {
        case "broilers":
            $scope.id = $rootScope.boilersNormalMedication;
            break;
        case "layers":
            $scope.id = $rootScope.layersNormalMedication;
            break;
        case "parents":
            $scope.id = $rootScope.parentsNormalMedication;
            break;
        default:

    }

    $scope.doRefresh = function() {

        // Send any parameters as supported by wordpress API passing argument as options in below service.
        wordpress.getPages($scope.id).then(function(objS) {
            objS = JSON.stringify(objS);
            objS = JSON.parse(objS);
            $scope.item = objS.data;

            $rootScope.item = $scope.item;
            $scope.$broadcast('scroll.refreshComplete');

        },function(err) {

            console.log(err);
            $scope.error = "Server error. Please reload";
            $scope.$broadcast('scroll.refreshComplete');

            return;
        });

    }

    $scope.doRefresh();
});