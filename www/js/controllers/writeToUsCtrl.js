app.controller('writeToUsCtrl', function($scope, $state, $rootScope, $ionicHistory, $cordovaEmailComposer, ionicMaterialInk, wordpress) {

    $scope.mail = {
    	name: "",
		email: "",
		phone: "",
		enquiry: "General Enquiry",
		message: ""
    }

    $scope.sendEmail = function (data) {
    	console.log(data);

    	$cordovaEmailComposer.isAvailable().then(function() {

    		var body = "<p>"+
                			"<b>Subject:</b> &nbsp;&nbsp;"+ "Farm App" +" <br>"+
                			"<b>Phone Number:</b> &nbsp;&nbsp;"+ data.phone +" <br>"+
                			"<b>Nature of Enquiry:</b> &nbsp;&nbsp;"+ data.enquiry +" <br><br>"+
                			"<b>Message:</b><br>"+
                			data.message
            			"</p>";
   			
    		var email = {
    			to: 'benimub@gmail.com',
    			cc: '',
			    bcc: [],
			    attachments: [],
    			subject: 'Farm App' ,
    			body: body,
    			isHtml: true
    		}; 

    		$cordovaEmailComposer.open(email).then(null, function () {
			   // user cancelled email
			});

    		$scope.mail = {
		    	name: "",
				email: "",
				phone: "",
				enquiry: "General Enquiry",
				message: ""
		    }

 		}, function () {
   			alert("No composer available");
		});
    }


});