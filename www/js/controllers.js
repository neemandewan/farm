app.controller('AppCtrl', function($scope, $state, $rootScope, $ionicHistory, $ionicSideMenuDelegate, $ionicPlatform, $cordovaNativeStorage, $ionicLoading, $timeout, $ionicPush, ionicMaterialInk, wordpress, ionicToast) {

    //ionic.meterial.ink.displayEffect();
    ionicMaterialInk.displayEffect();

    // Initialize home tab
    $rootScope.postsPerPage = 10;
    $rootScope.postsPageShown = 0;

    $rootScope.technicalPerPage = 10;
    $rootScope.technicalPageShown = 0;

    $rootScope.researchPerPage = 10;
    $rootScope.researchPageShown = 0;

    $rootScope.studentPerPage = 10;
    $rootScope.studentPageShown = 0;

    $rootScope.boilersVaccination = 3160;
    $rootScope.layersVaccination = 3162;
    $rootScope.parentsVaccination = 3167;

    $rootScope.boilersPoultryAnatomy = 3171;
    $rootScope.layersPoultryAnatomy = 3173;
    $rootScope.parentsPoultryAnatomy = 3175;

    $rootScope.boilersNormalMedication = 3177;
    $rootScope.layersNormalMedication = 3179;
    $rootScope.parentsNormalMedication = 3181;

    $rootScope.boilersDisease = 3183;
    $rootScope.layersDisease = 3186;
    // not done
    $rootScope.parentsDisease = 3189;

    $rootScope.boilersSpaceCalculator = 3191;
    $rootScope.layersSpaceCalculator = 3194;
    $rootScope.parentsSpaceCalculator = 3196;

    $rootScope.boilersLightMgmt = 3198;
    $rootScope.layersLightMgmt = 3200;
    $rootScope.parentsLightMgmt = 3202;

    $rootScope.boilersFeederWaterMgmt = 3204;
    $rootScope.layersFeederWaterMgmt = 3206;
    $rootScope.parentsFeederWaterMgmt = 3208;

    $rootScope.marketId = 3210;
    $rootScope.managementId = 3154;
    
    $rootScope.fbUrl = 'https://www.facebook.com/farm.com.np/';

    $rootScope.dashVal = null;

    $ionicPush.register().then(function(t) {
      try {

        $rootScope.tokenData = t;

        var deviceToken = t.token;
        var osType;
        var isAndroid = ionic.Platform.isAndroid();
        var prevToken;
        var opt;

        if(isAndroid) {
            osType = "android";
            os = "Android";
        } else {
            osType = "ios";
            os = "iOS";
        }

        $cordovaNativeStorage.getItem("token").then(function (value) {
            prevToken = value;

            opt = {
                token: deviceToken,
                os: os,
                prevToken: prevToken
            }

            $scope.sendPushReq(opt, $rootScope.tokenData);

        }, function (error) {

            opt = {
                token: deviceToken,
                os: os
            }
            
            $scope.sendPushReq(opt, $rootScope.tokenData);
        });

      }catch(e)  {
        console.log(e);
      }

      return;

    });

    /*$scope.$on('cloud:push:notification', function(event, data) {
        var msg = data.message;
        alert(msg.title + ': ' + msg.text);
    });*/

    $scope.sendPushReq = function(opt, t) {

        wordpress.postPushToken(opt).then(function(objS) {

            $cordovaNativeStorage.setItem("token", t.token).then(function (value) {

            }, function (error) {
                console.log(error);
            });

        },function(err) {
            console.log(err);
        });

        /*wordpress.sendPushToken(deviceToken, osType).then(function(objS) {

        },function(err) {
            console.log(err);
        });*/
    }

    $scope.exitApp = function () {
        ionic.Platform.exitApp();
    }

    $scope.logout = function () {
        $ionicLoading.show({
            template: '<ion-spinner icon="ripple"></ion-spinner>'
        });

        $timeout(function () {
            $rootScope.customerData = [];

            $ionicHistory.nextViewOptions({
                disableBack: true
            });

            $ionicSideMenuDelegate.toggleLeft();

            $timeout(function() {
                $ionicLoading.hide();
                ionicToast.show('Logged out successfully.', 'bottom', false, 3000);
                $state.go('app.home');
                
            }, 1000);

        }, 2000);
    }

    $scope.openLink = function () {
        window.open($rootScope.fbUrl, '_system');
    }
});